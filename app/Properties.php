<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\User;
use Carbon\Carbon;

class Properties extends Model
{
    use SoftDeletes;

    public function users()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(!Auth::guest()) {
                $model->users_id = Auth::user()->id;
            }
        });
    }

}
