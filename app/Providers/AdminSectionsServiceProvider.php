<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use SleepingOwl\Admin\Navigation\Page;
use AdminSection;
use PackageManager;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Properties::class => 'App\Http\Properties',
        \App\Tenancy::class => 'App\Http\Tenancy',
        \App\Tenants::class => 'App\Http\Tenants',
    ];

    private function registerNavigation()
    {
        \AdminNavigation::setFromArray([
            (new Page())->setTitle('Back to the site')->setUrl('home')->setPriority(0)->setIcon('fa fa-arrow-left'),
            (new Page(\App\Properties::class))->setPriority(1)->setIcon('fa fa-home'),
            (new Page(\App\Tenancy::class))->setPriority(2)->setIcon('fa fa-calendar'),
            (new Page(\App\Tenants::class))->setPriority(3)->setIcon('fa fa-group'),
        ]);
    }

    private function registerNRoutes()
    {
        $this->app['router']->group(['prefix' => config('sleeping_owl.url_prefix'), 'middleware' => config('sleeping_owl.middleware')], function ($router) {
            $router->get('', ['as' => 'admin.dashboard', function () {
                $content = 'Define your dashboard here.';
                return AdminSection::view($content, 'Dashboard');
            }]);
        });
    }

    private function registerMediaPackages()
    {

    }
    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//
        parent::boot($admin);

        $this->registerNRoutes();
        $this->registerNavigation();
        $this->registerMediaPackages();

    }
}
