<?php

namespace App\Http;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

class Tenancy extends Section
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'Tenancy';
    }
    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('users.name')->setLabel('Author'),
                AdminColumn::text('properties.name')->setLabel('Property'),
                AdminColumn::lists('tenants.name', 'Tenants'),
                AdminColumn::datetime('start')
                    ->setLabel('Start date')->setFormat('d.m.Y'),
                AdminColumn::datetime('end')
                    ->setLabel('End date')->setFormat('d.m.Y'),
                AdminColumn::text('rent', 'Monthly rent')
            )->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::select('properties_id', 'Property')
                ->setModelForOptions(new \App\Properties)
                ->setDisplay('name')->required(),
            AdminFormElement::multiselect('tenants', 'Tenants')->setModelForOptions(new \App\Tenants)->setDisplay('name'),
            AdminFormElement::date('start', 'Start date')->required(),
            AdminFormElement::date('end', 'End date')->required(),
            AdminFormElement::text('rent', 'Monthly rent')->required()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
