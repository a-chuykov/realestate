<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\User;
use Carbon\Carbon;

class Tenants extends Model
{

    public function users()
    {
        return $this->belongsTo(User::class, 'users_id');
    }
    public function tenancy()
    {
        return $this->belongsToMany('App\Tenancy', 'tenants_tenancy', 'tenants_id', 'tenancy_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(!Auth::guest()) {
                $model->users_id = Auth::user()->id;
            }
        });
    }

}
