<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\User;
use Carbon\Carbon;

class Tenancy extends Model
{
    protected $table = 'tenancy';

    use SoftDeletes;

    public function users()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function properties()
    {
        return $this->belongsTo(\App\Properties::class, 'properties_id');
    }

    public function tenants()
    {
        return $this->belongsToMany('App\Tenants', 'tenants_tenancy', 'tenancy_id', 'tenants_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(!Auth::guest()) {
                $model->users_id = Auth::user()->id;
            }
        });
    }
}
