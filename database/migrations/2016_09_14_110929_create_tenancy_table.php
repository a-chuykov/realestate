<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenancyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenancy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('properties_id')->unsigned();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->decimal('rent', 10, 0)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tenants_tenancy', function (Blueprint $table) {

            $table->integer('tenants_id')->unsigned();
            $table->integer('tenancy_id')->unsigned();

            $table->foreign('tenants_id')->references('id')->on('tenants')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tenancy_id')->references('id')->on('tenancy')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['tenants_id', 'tenancy_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenancy');
    }
}
